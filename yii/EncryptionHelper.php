<?php

namespace app\components;

use Yii;

namespace app\components;

use Yii;

class EncryptionHelper
{
    // Define the encryption method and key
    private static $cipherMethod = 'aes-256-cbc'; // Encryption method
    private static $key = 'your-secure-key'; // Secret encryption key. Replace with a secure value or load from config
    
    // Encrypt data
    public static function encrypt($data)
    {
        $ivLength = openssl_cipher_iv_length(self::$cipherMethod);
        $iv = openssl_random_pseudo_bytes($ivLength); // Generate a random IV
        $encrypted = openssl_encrypt($data, self::$cipherMethod, self::$key, 0, $iv);

        // Combine IV and encrypted data, then encode in base64
        $encryptedData = base64_encode($iv . $encrypted);

        // URL encode the encrypted data to safely pass it in a URL
        return urlencode($encryptedData);
    }

    // Decrypt data
    public static function decrypt($encryptedData)
    {
        // URL decode the encrypted data from the URL
        $encryptedData = urldecode($encryptedData);

        $ivLength = openssl_cipher_iv_length(self::$cipherMethod);

        // Decode base64 and extract IV and encrypted data
        $data = base64_decode($encryptedData);
        $iv = substr($data, 0, $ivLength);
        $cipherText = substr($data, $ivLength);

        return openssl_decrypt($cipherText, self::$cipherMethod, self::$key, 0, $iv);
    }
}
